//general includes
#include <math.h>

//Messages
#include "droneMsgsROS/droneNavCommand.h"
#include "std_msgs/Int16.h"

#include "droneMsgsROS/setControlMode.h"

//other resources
#include "control/PID.h"
#include "control/LowPassFilter.h"

//OpenCV
#include <opencv2/highgui/highgui.hpp>

#include "xmlfilereader.h"

#define MULTIROTOR_IBVSCONTROLLER_TS                    (1.0/15.0)
#define MULTIROTOR_IBVSCONTROLLER_INIT_CONTROLMODE   CTRL_NOT_STARTED


class DroneIBVSController
{
public:
        DroneIBVSController();
        ~DroneIBVSController();

public: // control mode definition
      enum controlMode
    {
        CTRL_NOT_STARTED = 1
        , PERSON_FOLLOWING = 2
        , PERSON_NOT_ON_FRAME = 3
   };

private:
    bool controller_is_started;
    controlMode control_mode;

protected:
     bool setControlModeVal(controlMode mode);

public:
    controlMode getControlMode();

protected:
    controlMode controlModeFromInt(int controlMode_int);
    int         controlModeToInt(controlMode controlMode_enum);


private:
    droneMsgsROS::droneNavCommand droneNavCommandMsg;

protected:
    void setNavCommand(float roll, float pitch, float dyaw, float dz, double time=-1.0);
    void setNavCommandToZero(void);

public:

    droneMsgsROS::droneNavCommand getNavCommand();

private:

     double xci,yci,ywaci,zci;
     double pitchfi, rollfi;
     double vxfi, vyfi, dyawfi, dzfi;

     double pitchco_hf, rollco_hf, dyawco_hf, dzco_hf;                 // hf ~ "high frequency", before (low pass) filtering
     double pitchco, rollco, dyawco, dzco;                            // co ~ controller output, sent to the AR Drone


     CVG_BlockDiagram::LowPassFilter pitch_lowpassfilter, roll_lowpassfilter, dyaw_lowpassfilter, dz_lowpassfilter;
     CVG_BlockDiagram::PID   pid_fx2R, pid_fx2DY, pid_fy, pid_fs, pid_fD;
     double C_fx2Dy, C_fx2DY, C_fy2Dz, C_fD2Dx;               // Constant to calculate distance2target from fxs, fys, fDs
     double C_DY2Dfx, C_DP2Dfy;
     double yaw_t, pitch_t, roll_t;      // telemetry
     double yaw_cr, pitch_cr, roll_cr;   // centroid reference
     double C_ObjVisArea;                        // m^2
     double Dxs, Dys, Dzs, DYs; // estimated distance to target

public:
     bool start();
     void init(std::string configFile);
     bool close();
     bool resetValues();
     bool startValues();
     bool stopValues();
     bool run();

public:
     bool readConfigs(std::string configFile);

public:

     bool boundingBox2ImageFeatures( const int bb_x, const int bb_y, const int bb_width, const int bb_height,
                                   float &fx, float &fy, float &fs, float &fD, const bool target_is_on_frame_in );

private:
    // input layers
    float  fxs,  fys,  fss,   fDs;   // image feature measurements
    float  fxci,fxci_yaw, fyci, fsci,  fDci;   // image feature references
    bool   target_is_on_frame;
    // internals
    float Dfx, Dfy, Dfs, DfD;
    // Values after correction
    float Dfx_4DYC, Dfx_4DyC, Dfy_4DzC, DfD_4DxC;
    float fxs_4DyC, fxs_4DYC, fys_4DzC, fDs_4DxC;

    // output layers
    // defined above on Internal variables in the controller block diagram "block of the code"

public:
    void getImFeatReferences(         float &fxci_out,     float &fyci_out,    float  &fsci_out,     float &fDci_out);
    void getImFeatFeedback2PIDs( float &fxs_4DyC_out, float &fxs_4DYC_out, float &fys_4DzC_out, float &fDs_4DxC_out);
    bool setImFeatMeasurements( const float fxs_in,  const float fys_in,  const float fss_in,  const float fDs_in, const int bb_width, const int bb_heigth);
    void setTargetIsOnFrame( const bool target_is_on_frame_in);
    float distanceToTarget( const float fD);
    void setTelemetryAttitude_rad( double yaw_t_in, double pitch_t_in, double roll_t_in);
private:
    int bb_width, bb_heigth;
    bool setImFeatReferencesFromDPos( float Dxc, float Dyc, float Dzc, float Dyawc);

private:
    void updateDistanceToTarget();

public:
    std::string controlMode2String();

//Tuning parameters coming from XML
public:

//output_saturations
double MULTIROTOR_IBVSCONTROLLER_DYAWMAX;
double MULTIROTOR_IBVSCONTROLLER_DZMAX;
double MULTIROTOR_IBVSCONTROLLER_MAX_PITCH;
double MULTIROTOR_IBVSCONTROLLER_MAX_ROLL;

//low_pass_filter_configs
double MULTIROTOR_TILT_REFERENCE_CUTOFF_TR;
double MULTIROTOR_DYAW_REFERENCE_CUTOFF_TR;
double MULTIROTOR_DALT_REFERENCE_CUTOFF_TR;

//camera_constants
double MULTIROTOR_FRONTCAM_RESOLUTION_WIDTH;
double MULTIROTOR_FRONTCAM_RESOLUTION_HEIGHT;
double MULTIROTOR_FRONTCAM_HORIZONTAL_ANGLE_OF_VIEW;
double MULTIROTOR_FRONTCAM_VERTICAL_ANGLE_OF_VIEW;
double MULTIROTOR_FRONTCAM_ALPHAX;
double MULTIROTOR_FRONTCAM_ALPHAY;
double MULTIROTOR_IBVSCONTROLLER_INIT_DEPTH;
double MULTIROTOR_IBVSCONTROLLER_TARGET_INIT_SIZE;
double MULTIROTOR_FAERO_DCGAIN_SPEED2TILT;

//distance to target estimation constants
double MULTIROTOR_FRONTCAM_C_fx2Dy;
double MULTIROTOR_FRONTCAM_C_fx2DY;
double MULTIROTOR_FRONTCAM_C_fy2Dz;
double MULTIROTOR_FRONTCAM_C_fD2Dx;
double MULTIROTOR_FRONTCAM_C_DY2Dfx;
double MULTIROTOR_FRONTCAM_C_DP2Dfy;

//PID Gains
double MULTIROTOR_IBVSCONTROLLER_FX2R_DELTA_KP;
double FX2R_KP;
double FX2R_KI;
double FX2R_KD;
double MULTIROTOR_IBVSCONTROLLER_FX2R_KP;
double MULTIROTOR_IBVSCONTROLLER_FX2R_KI;
double MULTIROTOR_IBVSCONTROLLER_FX2R_KD;

double MULTIROTOR_IBVSCONTROLLER_FD2P_DELTA_KP;
double FD2P_KP;
double FD2P_KI;
double FD2P_KD;
double MULTIROTOR_IBVSCONTROLLER_FD2P_KP;
double MULTIROTOR_IBVSCONTROLLER_FD2P_KI;
double MULTIROTOR_IBVSCONTROLLER_FD2P_KD;

double MULTIROTOR_IBVSCONTROLLER_FX2DY_DELTA_KP;
double FX2DY_KP;
double FX2DY_KI;
double FX2DY_KD;
double MULTIROTOR_IBVSCONTROLLER_FX2DY_KP;
double MULTIROTOR_IBVSCONTROLLER_FX2DY_KI;
double MULTIROTOR_IBVSCONTROLLER_FX2DY_KD;

double MULTIROTOR_IBVSCONTROLLER_FY2DZ_DELTA_KP;
double FY2DZ_KP;
double FY2DZ_KI;
double FY2DZ_KD;
double MULTIROTOR_IBVSCONTROLLER_FY2DZ_KP;
double MULTIROTOR_IBVSCONTROLLER_FY2DZ_KI;
double MULTIROTOR_IBVSCONTROLLER_FY2DZ_KD;

double MULTIROTOR_IBVSCONTROLLER_FS2P_DELTA_KP;
double FS2P_KP;
double FS2P_KI;
double FS2P_KD;
double MULTIROTOR_IBVSCONTROLLER_FS2P_KP;
double MULTIROTOR_IBVSCONTROLLER_FS2P_KI;
double MULTIROTOR_IBVSCONTROLLER_FS2P_KD;

//Init Values
double MULTIROTOR_IBVSCONTROLLER_INITVAL_FX_ROLL;
double MULTIROTOR_IBVSCONTROLLER_INITVAL_FX_YAW;
double MULTIROTOR_IBVSCONTROLLER_INITVAL_FY;
double MULTIROTOR_IBVSCONTROLLER_INITVAL_FS;
double MULTIROTOR_IBVSCONTROLLER_INITVAL_FD;
double MULTIROTOR_IBVSCONTROLLER_SAFETY_DISTANCE_TO_IMAGE_BORDER;
double MULTIROTOR_IBVSCONTROLLER_SAFETY_TARGET_IMAGE_AREA;


};
